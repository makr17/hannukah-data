/*
  year of the rabbit is every 12 years
  1903 is probably a safe place to start
*/
select generate_series(1903, 2011, 12);
/*
 generate_series 
-----------------
            1903
            1915
            1927
            1939
            1951
            1963
            1975
            1987
            1999
            2011
(10 rows)
*/

/*
and Cancer is June 21 through July 23
*/
select
  id
  , name
  , birthdate
  , phone
from
  customers
where
  extract('year' from birthdate) in (1903, 1915, 1927, 1939, 1951, 1963, 1975, 1987, 1999, 2011)
  and ((extract('month' from birthdate) = 6
    and extract('day' from birthdate) between 21 and 30)
   or  (extract('month' from birthdate) = 7
    and extract('day' from birthdate) between 1 and 23)
      )
;
/*
but that returns far more than I was expecting

  id  |           name           | birthdate  |    phone     
------+--------------------------+------------+--------------
 1047 | Tammy Houston            | 1999-07-04 | 216-937-0321
 1206 | Sarah Hernandez          | 1951-06-26 | 682-287-2999
 1285 | Christina Juarez         | 1951-07-03 | 680-982-8910
 1506 | Joshua Harvey            | 1975-06-30 | 838-694-9628
 1703 | Vanessa Freeman          | 1963-07-05 | 315-955-0790
...
 8376 | Andrea Brewer            | 1951-07-16 | 747-436-6286
 8403 | Jennifer York            | 1939-07-21 | 332-847-5410
 8737 | Sam Greene               | 1987-06-30 | 838-267-1187
 8831 | Monica Barnes            | 1999-06-23 | 615-752-5264
 8900 | Nancy Gillespie          | 1951-06-28 | 332-305-8272
(71 rows)

We can remove women, since the rug was given to a man

but "in my neighborhood" is probably the better clue,
we can just skip over women in the sorted output

first we need to figure out where our rug contractor lives
*/
select * from customers where id = 1475;
/*
-[ RECORD 1 ]+------------------
id           | 1475
name         | Joshua Peterson
address      | 100-75 148th St
citystatezip | Jamaica, NY 11435
birthdate    | 1947-02-05
phone        | 332-274-4185
timezone     | America/New_York
lat          | 40.70895
long         | -73.80856
*/

-- and install earthdistance extension
-- note, hopped over to admin account to add the extension
create extension earthdistance cascade;
/*
NOTICE:  installing required extension "cube"
CREATE EXTENSION
*/

-- now take our original query and sort by distance from Joshua
select
  id
  , name
  , birthdate
  , phone
  , earth_distance(ll_to_earth(lat, long), ll_to_earth(40.70895, -73.80856))
from
  customers
where
  extract('year' from birthdate) in (1903, 1915, 1927, 1939, 1951, 1963, 1975, 1987, 1999, 2011)
  and ((extract('month' from birthdate) = 6
    and extract('day' from birthdate) between 21 and 30)
   or  (extract('month' from birthdate) = 7
    and extract('day' from birthdate) between 1 and 23)
      )
order by 5
limit 10
;
/*
  id  |       name       | birthdate  |    phone     |   earth_distance   
------+------------------+------------+--------------+--------------------
 2550 | Robert Morton    | 1999-07-08 | 917-288-9635 | 2177.5400901247554
 5215 | Thomas Martinez  | 1963-06-30 | 516-480-5214 | 2478.3218384078923
 2708 | Michael Lopez    | 1975-07-23 | 516-318-9071 | 4666.7235960146445
 7384 | Sara Soto        | 1975-07-20 | 838-205-9457 |  5422.048533473504
 7437 | Bethany Waller   | 1987-06-22 | 332-533-0704 | 5697.5780559890945
 8403 | Jennifer York    | 1939-07-21 | 332-847-5410 |  7013.047572086497
 3455 | Patricia Andrews | 1999-06-26 | 631-615-1444 |  7498.496526324814
 1506 | Joshua Harvey    | 1975-06-30 | 838-694-9628 |  7653.524365446495
 6944 | Brenda Gomez     | 1975-06-21 | 315-459-6223 |  7925.020768561992
 7397 | Nick White       | 1963-06-28 | 646-503-6906 |  8414.223425657647
(10 rows)
*/

/*
and Robert Morton (917-288-9635) works!
*/
