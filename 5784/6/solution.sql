-- clips every coupon
-- look for largest differential between item value and order total?
with coupon_value (customer_id, order_id, saved) as (
  select
    o.customer_id
    , o.id
    , sum(oi.qty * oi.unit_price) - o.total
  from
    orders o
    join order_items oi on o.id = oi.order_id
  group by 1,2
)
select
  c.id
  , c.name
  , c.phone
  , sum(saved)
from
  customers c
  join coupon_value cv on c.id = cv.customer_id
group by 1,2,3
having sum(saved) > 0
order by 4 desc
;
/*
 id | name | phone | sum 
----+------+-------+-----
(0 rows)

ok, so that isn't it
*/

-- maybe the clue is "Noah actually loses money whenever she comes in the store<
-- let's look at order total and wholesale cost
with below_wholesale (customer_id, order_id, sku, sold_at, wholesale, lost) as (
  select
    o.customer_id
    , o.id
    , oi.product_sku
    , oi.unit_price
    , p.wholesale_cost
    , oi.qty * (oi.unit_price - p.wholesale_cost)
  from
    orders o
    join order_items oi on o.id = oi.order_id
    join products p on oi.product_sku = p.sku
  where
    p.wholesale_cost > oi.unit_price
)
select
  c.id
  , c.name
  , c.phone
  , sum(lost)
from
  customers c
  join below_wholesale bw on c.id = bw.customer_id
group by 1,2,3
order by 4
limit 5
;
/*
  id  |        name        |    phone     |  sum   
------+--------------------+--------------+--------
 4167 | Sherri Long        | 585-838-9161 | -92.56
 8841 | Phillip Melton     | 315-810-6466 |  -5.23
 8606 | Daniel Salazar Jr. | 838-205-6543 |  -3.86
 4769 | Angela Miller      | 838-239-1913 |  -3.30
 5698 | Sam Smith          | 212-981-6539 |  -3.17
(5 rows)
*/
