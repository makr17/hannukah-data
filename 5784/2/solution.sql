-- I started by looking at customers with initials JP

with inits (id, init, phone) as (
select
  id
  , left(split_part(lower(name), ' ', 1), 1)
    || left(split_part(lower(name), ' ', 2), 1)
  , phone
from
  customers
)
select * from inits where init = 'jp'
;

-- but there were 71 of them
-- so let's take it from the coffee/bagel side
-- means we need to actually _load_ product and order data

create table products (
  sku  varchar not null primary key,
  description  varchar not null,
  wholesale_cost  numeric not null,
  dims_cm  varchar not null
);
\copy products from 'noahs-products.csv' csv header;
/*
CREATE TABLE
COPY 1278
*/

create table orders (
  id  integer not null primary key,
  customer_id  integer not null references customers(id),
  ordered  timestamp not null,
  shipped  timestamp not null,
  items  varchar,
  total  numeric not null
);
\copy orders from 'noahs-orders.csv' csv header;
/*
CREATE TABLE
COPY 213232
*/

create table order_items (
  order_id  integer not null references orders(id),
  product_sku  varchar not null references products(sku),
  qty  integer not null,
  unit_price  numeric not null,
  primary key (order_id, product_sku)
);
\copy order_items from 'noahs-orders_items.csv' csv header;
/*
CREATE TABLE
COPY 426541
*/

-- so let's find bagel and coffee skus

select * from products where description ilike '%bagel%';
/*
   sku   |  description  | wholesale_cost |   dims_cm    
---------+---------------+----------------+--------------
 BKY1573 | Sesame Bagel  |           1.02 | 11.9|4.7|0.9
 BKY5717 | Caraway Bagel |           1.03 | 11.3|2.3|1.6
(2 rows)
*/

select * from products where description ilike '%coffee%';
/*
sku   | description  | wholesale_cost |   dims_cm   
---------+--------------+----------------+-------------
 DLI8820 | Coffee, Drip |           1.44 | 9.6|7.8|0.7
(1 row)
*/

-- now let's see who has ordered coffee and bagel
select
  o.id
  , o.customer_id
  , o.ordered
  , array_agg(oi.product_sku)
from
  orders o
  join order_items oi on oi.order_id = o.id
group by 1,2
having 'DLI8820' = any(array_agg(oi.product_sku))
  and ('BKY1573' = any(array_agg(oi.product_sku)) or 'BKY5717' = any(array_agg(oi.product_sku)))
;
/*
 orderid | customerid |       ordered       |         array_agg         
---------+------------+---------------------+---------------------------
    7459 |       1475 | 2017-04-05 11:42:15 | {DLI8820,HOM2761,BKY1573}
(1 row)
*/

-- hmmm, only one?  and it _is_ in 2017
select id, name, phone from customers where id = 1475;
/*
 customerid |      name       |    phone     
------------+-----------------+--------------
       1475 | Joshua Peterson | 332-274-4185
(1 row)
*/

-- initials are JP, so we're done

-- explain plan wasn't great (full scan on orders_items)
-- index on (order_id, product_sku) helps

create index on order_items (order_id, product_sku);

-- but still not _great_
-- restructuring the query brings execution time way down (5ms vs 250ms)
-- I'm leaving in the join to oi and the array_agg so I can see the order(s) to visually verify
select
  o.id
  , o.customer_id
  , o.ordered
  , array_agg(oi.product_sku)
from
  orders o
  join order_items coffee on coffee.order_id = o.id and coffee.product_sku = 'DLI8820'
  join order_items bagel on bagel.order_id = o.id and bagel.product_sku in ('BKY1573','BKY5717')
  join order_items oi on oi.order_id = o.id
group by 1,2
;
/*
 orderid | customerid |       ordered       |         array_agg
---------+------------+---------------------+---------------------------
    7459 |       1475 | 2017-04-05 11:42:15 | {BKY1573,DLI8820,HOM2761}
(1 row)
*/
