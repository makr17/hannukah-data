-- woman, pastry order, pre-5am

-- start with all orders in 2017 shipped (picked up?) before 5am
select * from orders where extract('hour' from shipped) < 5 and extract('year' from shipped) = 2017;
/*
  id   | customer_id |       ordered       |       shipped       | items | total 
-------+-------------+---------------------+---------------------+-------+-------
  1298 |        2889 | 2017-02-03 00:43:52 | 2017-02-03 04:15:00 |       |  2.53
  1301 |        4233 | 2017-02-03 02:02:42 | 2017-02-03 03:15:00 |       | 39.51
  1414 |        5928 | 2017-02-04 00:19:19 | 2017-02-04 00:19:19 |       |  1.22
  1495 |        6020 | 2017-02-05 01:13:58 | 2017-02-05 03:00:00 |       |  2.20
  1796 |        8313 | 2017-02-08 00:20:51 | 2017-02-08 01:45:00 |       |  5.40
...
 33291 |        1463 | 2017-12-21 01:23:49 | 2017-12-21 02:15:00 |       |  2.48
 33505 |        6752 | 2017-12-23 02:48:12 | 2017-12-23 02:48:12 |       |  3.37
 33586 |        7870 | 2017-12-24 00:58:58 | 2017-12-24 00:58:58 |       |  1.76
 33587 |        5147 | 2017-12-24 01:05:08 | 2017-12-24 01:05:08 |       |  1.01
 34023 |        4208 | 2017-12-29 01:48:29 | 2017-12-29 02:30:00 |       | 11.12
(203 rows)
*/

-- let's look at what was ordered
-- and let's try limiting to bakery items (sku starts with BKY)
select
  o.id
  , o.customer_id
  , c.name
  , c.phone
  , o.shipped
  , array_agg(p.description)
from
  orders o
  join customers c on o.customer_id = c.id
  join order_items oi on o.id = oi.order_id
  join products p on oi.product_sku = p.sku and p.sku like 'BKY%'
where
  extract('hour' from o.shipped) < 5
  -- and extract('year' from o.shipped) = 2017
group by 1,2,3,4
;
/*
  id   | customer_id |       name       |    phone     |       shipped       |                  array_agg                  
-------+-------------+------------------+--------------+---------------------+---------------------------------------------
  2794 |        3903 | Lori Snow        | 914-870-9357 | 2017-02-17 04:15:00 | {"Raspberry Linzer Cookie","Caraway Twist"}
  7926 |        4254 | Brandon Castillo | 914-618-0663 | 2017-04-10 04:30:00 | {"Sesame Bialy"}
 12503 |        6878 | Ruben Bauer      | 914-585-2452 | 2017-05-26 03:00:00 | {"Sesame Bialy"}
 12872 |        4950 | Deborah Silva    | 716-277-6176 | 2017-05-30 01:51:32 | {"Caraway Puff"}
 19657 |        5067 | Sara Brown       | 315-825-4679 | 2017-08-06 03:30:00 | {"Sesame Bagel"}
 21265 |        4147 | Erin Jones       | 838-388-2668 | 2017-08-22 02:45:00 | {"Caraway Puff"}
(6 rows)

that narrows things down a lot
but none of the women in that result have phone numbers that work
*/

-- maybe look for repeat customers
-- "Apparently she liked to get up before dawn and claim the first pastries that came out of the oven."
select
  o.customer_id
  , c.name
  , c.phone
  , count(1)
from
  orders o
  join customers c on o.customer_id = c.id
  join order_items oi on o.id = oi.order_id
  join products p on oi.product_sku = p.sku and p.sku like 'BKY%'
where
  extract('hour' from o.shipped) < 5
group by 1,2,3
order by 4 desc
limit 5
;
/*
 customer_id |      name       |    phone     | count 
-------------+-----------------+--------------+-------
        2749 | Renee Harmon    | 607-231-3605 |     5
        7441 | Wanda Johnson   | 516-564-8084 |     2
        1866 | Scott Hayes II  | 631-923-6868 |     2
        3903 | Lori Snow       | 914-870-9357 |     2
        1368 | Alicia Robinson | 914-727-2760 |     1
(5 rows)

works, Renee Harmon is our bicycle chain repairer
*/
