-- start by loading the customer data into postgres
create table customers (
  id integer not null primary key,
  name varchar not null,
  address varchar not null,
  citystatezip varchar not null,
  birthdate date not null,
  phone varchar not null,
  timezone varchar not null,
  lat numeric not null,
  long numeric not null
);

\copy customers from 'noahs-customers.csv' csv header;

/*
quick and dirty
use split_part() to grab the second string from the name
  we'll call that last_name or lname
  find lnames of length 10 (since we're trying to match a phone number)
  and use translate() to turn lname into a phone number to match against phone
    remember, no 'q' or 'z' on a dialpad
    use translate() again to remove the '-' from phone numbers for matching
*/
with last_name (id, lname, phone) as (
  select id, split_part(name, ' ', 2), phone from customers
)
, dialpad (id, lname, phone, dialpad_name) as (
  select
    id
    , lname
    , phone
    , translate(
        lower(lname),
        'abcdefghijklmnoprstuvwxy',
        '222333444555666777888999'
      )
  from last_name
  where length(lname) = 10
)
select * from dialpad where dialpad_name = translate(phone, '-', '');

/*
  id  |   lname    |    phone     | dialpad_name 
------+------------+--------------+--------------
 1208 | Tannenbaum | 826-636-2286 | 8266362286
(1 row)
*/
