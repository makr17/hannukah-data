-- Staten Island
-- Noah's sweatshirt
-- has 10 or 11 _old_ cats

-- hmmm, no sweatshirt
select * from products where description ilike '%sweat%';
/*
 sku | description | wholesale_cost | dims_cm 
-----+-------------+----------------+---------
(0 rows)
*/

-- same for Noah's Market
select sku, description from products where description ilike '%noah''s market%' order by sku;
/*
 sku | description 
-----+-------------
(0 rows)
*/

-- but there are _jerseys_
select sku, description from products where description ilike '%jersey%' order by sku;
/*
   sku   |       description       
---------+-------------------------
 COL2466 | Noah's Jersey (magenta)
 COL2615 | Noah's Jersey (green)
 COL3655 | Noah's Jersey (amber)
 COL4281 | Noah's Jersey (yellow)
 COL4285 | Noah's Jersey (purple)
 COL5184 | Noah's Jersey (puce)
 COL5848 | Noah's Jersey (red)
 COL6461 | Noah's Jersey (white)
 COL6858 | Noah's Jersey (blue)
 COL7454 | Noah's Jersey (mauve)
 COL7568 | Noah's Jersey (azure)
 COL9064 | Noah's Jersey (orange)
(12 rows)
*/

-- and we have State Island addresses it citystatezip
select citystatezip, count(1) from customers where citystatezip ilike '%staten%' group by 1;
/*
      citystatezip       | count 
-------------------------+-------
 Staten Island, NY 10304 |    22
 Staten Island, NY 10307 |    11
 Staten Island, NY 10301 |    27
 Staten Island, NY 10306 |    43
 Staten Island, NY 10309 |    21
 Staten Island, NY 10310 |    16
 Staten Island, NY 10308 |    18
 Staten Island, NY 10303 |    20
 Staten Island, NY 10302 |    16
 Staten Island, NY 10314 |    65
 Staten Island, NY 10312 |    50
 Staten Island, NY 10305 |    23
(12 rows)
*/

-- putting it all together
select
  c.name
  , c.phone
from
  customers c
where
  citystatezip like 'Staten Island%'
  and exists (
    select 1
    from
      orders o
      join order_items oi on o.id = oi.order_id
      join products p on oi.product_sku = p.sku and p.description like 'Noah''s Jersey%'
  )
  and exists (
    select 1
    from
      orders o
      join order_items oi on o.id = oi.order_id
      join products p on oi.product_sku = p.sku and p.description like '%Senior Cat%'
  )
;
/*
not restrictive enough

            name            |    phone     
----------------------------+--------------
 Jennifer Oconnor           | 914-349-9960
 Tammy Nelson               | 929-715-7490
 Raymond House V            | 607-982-1214
 Robert Mejia               | 631-961-3677
 John Clarke                | 516-338-3885
...
 Thomas Johnson             | 917-540-0444
 Cathy Mack                 | 516-463-1103
 Jesse Olson                | 516-436-7078
 Vickie Schwartz            | 585-208-5694
 Devin Smith                | 315-834-4549
(332 rows)
*/

-- "plastic bag" isn't the clue
select sku, description from products where description ilike '%plastic%' order by sku;
/*
 sku | description 
-----+-------------
(0 rows)

and every match for '%bag%' is food.  Bagel or Cabbage.
*/

-- let's look at customers who buy a lot of senior cat food
select
  c.id
  , c.name
  , c.phone
--  , p.sku
--  , p.description
  , count(1)
from
  customers c
  join orders o on c.id = o.customer_id
  join order_items oi on o.id = oi.order_id
  join products p on oi.product_sku = p.sku
where
  c.citystatezip like 'Staten Island%'
  and p.description like '%Senior Cat%'
group by 1,2,3
order by 4 desc
;
/*
  id  |         name          |    phone     | count 
------+-----------------------+--------------+-------
 3068 | Nicole Wilson         | 631-507-6048 |    21
 2996 | Dawn Shelton          | 680-260-3507 |     6
 3408 | Kayla Solis           | 929-877-1866 |     6
 2345 | Brian Hudson III      | 516-570-4577 |     6
 7762 | Sean Wade Jr.         | 347-916-9069 |     5
...

and Nicole is in fact our freecycler
*/
