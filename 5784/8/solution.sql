-- man who owns a full set of noah's collectibles
-- so sku like 'COL%'
select count(1) from products where sku like 'COL%';
/*
 count 
-------
    85
(1 row)
*/

select
  c.id
  , c.name
  , c.phone
  , count(distinct oi.product_sku)
from
  customers c
  join orders o on c.id = o.customer_id
  join order_items oi on o.id = oi.order_id
where
  oi.product_sku like 'COL%'
group by 1,2,3
order by 4 desc
limit 5
;
/*
  id  |         name         |    phone     | count 
------+----------------------+--------------+-------
 3808 | James Smith          | 212-547-3518 |    85
 4254 | Brandon Castillo     | 914-618-0663 |    31
 6855 | Elizabeth Reyes      | 516-993-9571 |    30
 3914 | Hailey Fisher        | 838-838-1754 |    30
 8665 | Stephen Williams Jr. | 914-300-2614 |    30
(5 rows)
*/
