-- same item, same-ish time, different color
-- look at skus that have color (all seem to be items in the COL namespace
-- and strip out the colors so we can match "same" items
select
  sku
  , description
  , regexp_replace(description, ' \(\w+\)', '')
from
  products
where
  sku like 'COL%'
order by sku;
/*
   sku   |          description           |       regexp_replace        
---------+--------------------------------+-----------------------------
 COL0037 | Noah's Jewelry (green)         | Noah's Jewelry
 COL0041 | Noah's Ark Model (HO Scale)    | Noah's Ark Model (HO Scale)
 COL0065 | Noah's Jewelry (mauve)         | Noah's Jewelry
 COL0166 | Noah's Action Figure (blue)    | Noah's Action Figure
 COL0167 | Noah's Bobblehead (blue)       | Noah's Bobblehead
...
 COL9349 | Noah's Action Figure (orange)  | Noah's Action Figure
 COL9420 | Noah's Jewelry (amber)         | Noah's Jewelry
 COL9773 | Noah's Poster (magenta)        | Noah's Poster
 COL9819 | Noah's Lunchbox (blue)         | Noah's Lunchbox
 COL9948 | Noah's Jewelry (magenta)       | Noah's Jewelry
(85 rows)
*/

with strip_colors (sku, description) as (
  select
    sku
    , regexp_replace(description, ' \(\w+\)', '')
  from
    products
  where
    sku like 'COL%'
    and description ~ ' \(\w+\)'
)
select
  c1.id
  , c2.id
  , c2.name
  , c2.phone
  , array_agg(sc1.description)
  , array_agg(sc2.description)
from
  customers c1
  join orders o1 on c1.id = o1.customer_id
  join order_items oi1 on o1.id = oi1.order_id
  join strip_colors sc1 on oi1.product_sku = sc1.sku
  join strip_colors sc2 on sc1.description = sc2.description
  join order_items oi2 on sc2.sku = oi2.product_sku
  join orders o2 on oi2.order_id = o2.id
  join customers c2 on o2.customer_id = c2.id
where
  c1.id = 4167 -- from yesterday
  and c1.id <> c2.id
  and abs(extract('epoch' from o1.shipped - o2.shipped)) < 600 -- 10 minutes
group by 1,2,3,4
;
/*
  id  |  id  |     name     |    phone     |     array_agg     |     array_agg     
------+------+--------------+--------------+-------------------+-------------------
 4167 | 5783 | Carlos Myers | 838-335-7157 | {"Noah's Poster"} | {"Noah's Poster"}
(1 row)
*/
